# Description of src code

* The following is a brief description of the source code
    * The following program was implemented on an Arduino Uno and programmed in the arduino langauge.
    * All of the control was implemented by myself and was not imported from any preexisting libraries.
    * Libraries were imported for the camera, IR remote, and Servo commands.

## Contents / functionality
* The following describes the contents and functionality of the src code:
1. pid class to contain PID information, calculations, and outputs
2. circle_path class to contain and calculate the path to move the ball in a circle
3. Main loop:
    1. Check for IR input
    2. Update every 1/30 of a second
    3. Read camera result
    4. Calculate PID output
    5. Send output to servo command.
4. IR remote commands.
