#include <Servo.h>
#include <Pixy2.h>
#include <IRremote.h>

//Visit pixy website for camera documentation


//Struct to hold the PID related values
class pid {
  public:
  String name;
  //Controller Gains
  float kp, ki, kd;
  
  //Sample time
  float T;
  float tau;
  
  //Controller Memory
  float integrator;
  float prev_error;
  float differentiator;
  
  //Output limitations
  float maxLim;
  float minLim;

  //Limit on integrator - anti windup
  float integrator_max;
  float integrator_min;
  
  //Output
  float output;

  pid(String _name, int _kp, int _ki, int _kd, float _max, float _min)
  {
    kp = _kp;
    ki = _ki;
    kd = _kd;
    T = 0.016666666;
    tau = 0.15;
    integrator = 0;
    prev_error = 0;
    differentiator = 0;
    maxLim = _max;
    minLim = _min;
    integrator_max = 500;
    integrator_min = -500;
    
    output = 0;
    name = _name;
    }

  void resetPID()
    {
      integrator = 0;
      prev_error = 0;
      differentiator = 0;
    }

  float update_PID(float setpoint, float measurement)
  {
    
    float error = setpoint - measurement;

    //Calculate the Proportional Part
    /////////////////////////////////
    float proportional = kp * error;


    //Calculate the Integral Part
    /////////////////////////////
    integrator = integrator + 0.5 * ki * T * (error + prev_error);

    //Care for max limits, integrator windup
    if(integrator > integrator_max) {
      
      integrator = integrator_max;
      }
      else if (integrator < integrator_min){
        integrator = integrator_min;
        }

    //Calculate the Derivative Part
    ///////////////////////////////

      differentiator = ( 2.0 * kd * (error - prev_error)
                            + (2.0 * tau - T) * differentiator ) / (2.0 * tau + T);
      
    //Calculate the output!
    ///////////////////////
    output = proportional + integrator + differentiator;

      if(output > maxLim) {
      
      output = maxLim;
      }
      
      else if (output < minLim){
        output = minLim;
        }

    //Set the previous terms!
    prev_error = error;

    return output;
      
    }
};  //End PID Class


//Clsss to help with circle path calculations
class circle_path
{
    public:
    int c_x, c_y, radius, points, x_set, y_set;
    float cut;
    String name;
    
  circle_path(String _name)
    {
    name = _name;
    c_x = 140;
    c_y = 125;
    radius = 60;
    points = 64;

    cut = 2 * 3.14159 / points;
    }
  
    void calc_setpoint(int counter)
    {
      
     float angle = cut * counter;
     x_set = (int)(c_x + radius * cos(angle));
     y_set = (int)(c_y + radius * sin(angle));  
      
      }      
    }; //End circle_path class


//Instantiate pid objects
//Best
pid x_pid("x_pid", 4.0, 0, 9, 2000, -2000);
pid y_pid("y_pid", 6.07, 0, 10, 2000, -2000);

//Tester PID's
//pid x_pid("x_pid", 5.0, 0, 14, 2000, -2000);
//pid y_pid("y_pid", 7.07, 0, 10, 2000, -2000);

//Timer variables
unsigned long previousMillis = 0;
unsigned long interval = 10UL; 
unsigned long cir_previousMillis = 0;
unsigned long cir_interval = 250UL;

//Main Pixy object 
Pixy2 pixy;
float x, y;

//Servo Objects
Servo x_servo;
Servo y_servo;

//Remote Variables
int RECV_PIN = 6;
IRrecv irrecv(RECV_PIN);
decode_results results;

//instantiate circle path object
circle_path myPath("Circle Path");


void setup() {
  // put your setup code here, to run once:

  Serial.begin(115200);
  Serial.print("Starting...\n");
  //pid myPid("x_pid");
  x_pid.output = 0;
  y_pid.output = 0;

  pixy.init();

  irrecv.enableIRIn(); // Start the receiver

  x_servo.attach(9);
  y_servo.attach(10);

  x_servo.write(95);
  y_servo.write(95);
 
}

float x_output, y_output;

//Middle point
float x_setpoint = 140.00;
float y_setpoint = 125.00;

bool runPID = true;

bool circlePath = false;
int circle_count = 0;

void loop() {

  //If remote signal is recieved
  if (irrecv.decode(&results))
  {
   Serial.println(results.value, HEX);
   setpointSelect();
   irrecv.resume(); // Receive the next value
  } 

  //If going in a circle is selected
  if(circlePath)
  {
    if (circle_count == 64) //64 setpoints in circle path
    {
      circle_count = 0;
    } 

   //Timer to update the circle setpoint every 1/4 second.
   if(millis() - cir_previousMillis >= cir_interval)
   { 
      cir_previousMillis += cir_interval;
      
      myPath.calc_setpoint(circle_count);
      x_setpoint = myPath.x_set;
      y_setpoint = myPath.y_set;
      circle_count++;
    }
  }//end CirclePath  

  //Timer to calculate pid every 0.01s
  if (millis() - previousMillis >= interval)
  {
   previousMillis += interval;  //get ready for the next iteration

   pixy.ccc.getBlocks(true, 1, 1);  //grab blocks

   //If camera has detected an object:
   if(runPID){
     if (pixy.ccc.numBlocks)
      {
  
        if (pixy.ccc.blocks[0].m_age > 10)
          {
          y = pixy.ccc.blocks[0].m_y;
          //Serial.print("the y coord is: ");
          //Serial.println(y);
          x = pixy.ccc.blocks[0].m_x;
          //Serial.print("the x coord is: ");
          //Serial.println(x);
    
          x_output = x_pid.update_PID(x_setpoint, x);
          y_output = y_pid.update_PID(y_setpoint, y);

          //Scaler variable from output to servo command
          x_servo.write(90+(round(x_output/66)));
          y_servo.write(90-(round(y_output/66)));
        }
  
        
      } else { //reset the pid and servos if the object is lost
       
        x_pid.resetPID();
        y_pid.resetPID();
        x_servo.write(90);
        y_servo.write(93); 
        }
        
   }//end runPID
    
  } //end timer
  
} //end loop

//IR cammers commands
void setpointSelect()
{
  Serial.println(results.value, HEX);
  
    switch(results.value)
    {
      
  //Turn off the controller POWER
  case 0xFFA25D:
    runPID = false;
    circlePath = false;
    x_pid.resetPID();
    y_pid.resetPID();
    x_servo.write(90);
    y_servo.write(93);
    break;
      
  //Center Point 0
  case 0xFF6897:
    runPID = true;
    circlePath = false;
    x_pid.resetPID();
    y_pid.resetPID();
    x_setpoint = 140.00;
    y_setpoint = 125.00;
    break;

  //Back Right 6
  case 0xFF5AA5:
    runPID = true;
    circlePath = false;
    x_pid.resetPID();
    y_pid.resetPID();
    x_setpoint = 80.00;
    y_setpoint = 165.00;
    break;
    
  //Back Left 7
  case 0xFF42BD:
    runPID = true;
    circlePath = false;
    x_pid.resetPID();
    y_pid.resetPID();
    x_setpoint = 230.00;
    y_setpoint = 170.00;
    break;

  //Front Left 8
  case 0xFF4AB5:
    runPID = true;
    circlePath = false;
    x_pid.resetPID();
    y_pid.resetPID();
    x_setpoint = 200.00;
    y_setpoint = 100.00;
    break;

  //Front Right 9
  case 0xFF52AD:
    runPID = true;
    circlePath = false;
    x_pid.resetPID();
    y_pid.resetPID();
    x_setpoint = 75.00;
    y_setpoint = 90.00;
    break;

  //CirclePath 1
  case 0xFF30CF:
    runPID = true;
    x_pid.resetPID();
    y_pid.resetPID();
    circlePath = true;
    break;
    
  default:
    Serial.println("  Other button pressed: ignore!");  
      }
  }
