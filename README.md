# PD Balance Board

PD controlled balance board.  This board balances the ping-pong ball to the desired location while adjusting and reacting to external stimuli.

## Youtube link
https://youtube.com/shorts/I6lsBxcjfOc?feature=share

![image info](./BalanceBoard_1.jpg)

## Contents
1. Images of constructed balance board
2. Youtube video link (See above)
3. Src code for the programming.

## Description
This balancing board uses a PD (Proportional Derivative) control algorithm to balance the ping-pong ball to a set location.  The physical location of the ball is determined from the camera and the difference is taken from the desired set location.  This difference is sent to the PD algorithm which sends an output number which can be translated into a servo command. This calculation is done 30 times per second done for both the X and Y axis.

An IR remote can be used to change the set location of the ball.  The avaliable locations are:
1. Center
2. Each of the 4 corners
3. A circular path


Tyler George
5/24/2022
